import React, { useRef } from 'react'
import styles from './NavBar.module.css'
import { NavLink } from 'react-router-dom'

const NavBar: React.FC = () => {

  const searchInputRef = useRef<HTMLInputElement>(null)

  return (
    <div className={styles.navBar}>
      <div className={styles.navBarLeft}>
        <div className={styles.logoContainer}>
            <h2>My Blog</h2>
        </div>
        </div>
        <div className={styles.navBarRight}>
        {true && (
          <input
            autoFocus={true}
            ref={searchInputRef}
            className={styles.searchInput}
            defaultValue={''}
            placeholder={'search'}
          />
        )}
        <div className={styles.links}>
          <NavLink
            activeClassName={styles.activeLink}
            to={'/'}
          >
            Link 1
          </NavLink>
          <NavLink activeClassName={styles.activeLink} to={'/'}>
           Link 2
          </NavLink>
          <NavLink activeClassName={styles.activeLink} to={'/'}>
           Link 3
          </NavLink><NavLink activeClassName={styles.activeLink} to={'/'}>
           Profile
          </NavLink><NavLink activeClassName={styles.activeLink} to={'/'}>
           Logout
          </NavLink>
        </div>
      </div>
    </div>
  )
}

export default NavBar
