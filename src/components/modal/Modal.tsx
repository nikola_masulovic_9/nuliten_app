import React from "react";
import { useState } from "react";
import { postBlog } from "service/blogService";
import styles from "./Modal.module.css";

interface modalProps {
  onClose():void
}

const Modal: React.FC<modalProps> = ({onClose}) => {
  const [title, setTitle] = useState("");
  const [text, setText] = useState("");

  const postBlogAction = (title:string, text:string) => {
    postBlog(title,text)
  }

  return (
    <div className={styles.modal} id="modal">
      <h2>Add Blog</h2>
      <div className={styles.content}>
        <p>Title</p>
        <input
          value={title}
          name="title"
          onChange={(e) => {
            setTitle(e?.target?.value);
          }}
        ></input>
        <p>Text</p>
        <input
          type="text"
          value={text}
          onChange={(e) => {
            setText(e?.target?.value);
          }}
        ></input>
      </div>
      <div className={styles.actions}>
        <button className="toggle-button" onClick={() => {
          postBlogAction(title,text)
        }}>Post</button>
        <button className="toggle-button" onClick={() => {
          onClose()
        }}>close</button>
      </div>
    </div>
  );
};

export default Modal;
