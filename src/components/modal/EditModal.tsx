import React from "react";
import { useState } from "react";
import { editBlog } from "service/blogService";
import { IBlog } from "service/interfaces";
import styles from "./Modal.module.css";

interface modalProps {
  onClose():void
  blog:IBlog
}

const EditModal: React.FC<modalProps> = ({onClose,blog}) => {
  const [title, setTitle] = useState(blog.title);
  const [text, setText] = useState(blog.text);

  const postBlogAction = () => {
    const editedBlog:IBlog = blog
    editedBlog.text = text
    editedBlog.title = title
    editBlog(editedBlog)
  }

  return (
    <div className={styles.modal} id="modal">
      <h2>Edit</h2>
      <div className={styles.content}>
        <p>Title</p>
        <input
          value={title}
          name="title"
          onChange={(e) => {
            setTitle(e?.target?.value);
          }}
        ></input>
        <p>Text</p>
        <input
          type="text"
          value={text}
          onChange={(e) => {
            setText(e?.target?.value);
          }}
        ></input>
      </div>
      <div className={styles.actions}>
        <button className="toggle-button" onClick={() => {
          postBlogAction()
        }}>Post</button>
        <button className="toggle-button" onClick={() => {
          onClose()
        }}>close</button>
      </div>
    </div>
  );
};

export default EditModal;
