import React, { useRef, MutableRefObject } from 'react'
import styles from './SideBar.module.css'
import { NavLink } from 'react-router-dom'


const SideBar: React.FC = () => {

  const navRef: MutableRefObject<HTMLDivElement | null> = useRef<HTMLDivElement | null>(
    null
  )

  return (
    <div ref={navRef} className={styles.sideBar}>
      <p>Categories</p>
      <div className={styles.categories}>
      <NavLink to='/'>Category 1</NavLink>
      <NavLink to='/'>Category 2</NavLink>
      <NavLink to='/'>Category 3</NavLink>
      </div>
    </div>
  )
}

export default SideBar
