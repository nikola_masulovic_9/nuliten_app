import React from 'react'
import { IBlog } from 'service/interfaces'
import styles from './Blog.module.css'

interface BlogProps {
  blog:IBlog
  deleteAction(id:number): void
  editAction(): void
}

const Blog: React.FC<BlogProps> = ({blog,deleteAction,editAction}) => {

  return (
    <div className={styles.blog}>
      <div className={styles.blogTop}>
      <div className={styles.blogHeader}>
        <img className={styles.blogImg}
        src={'https://www.btklsby.go.id/images/placeholder/basic.png'}
        alt={'ffd'}></img>
        <div className={styles.blogHeaderDesc}>
        <p>{blog.title}</p>
        <p>{blog.createdAt}</p>
        </div>
      </div>
      <div className={styles.blogActions}>
        <button onClick={() => editAction()} >Edit</button>
        <button onClick={() => deleteAction(blog.id)}>Delete</button>
      </div>
      </div>
      <div className={styles.blogBody}>
        <div>{blog.text}</div>
        <img className={styles.blogImg}
        src={'https://www.btklsby.go.id/images/placeholder/basic.png'}
        alt={'ffd'}></img>
        <img className={styles.blogImg}
        src={'https://www.btklsby.go.id/images/placeholder/basic.png'}
        alt={'ffd'}></img>
        <img className={styles.blogImg}
        src={'https://www.btklsby.go.id/images/placeholder/basic.png'}
        alt={'ffd'}></img>
      </div>
    </div>
  )
}

export default Blog
