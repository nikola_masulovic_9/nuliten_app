import axios from 'axios'

export const baseUrl = 'https://frontend-api-test-nultien.azurewebsites.net/'

export const http = axios.create({
  headers: {
    'X-Requested-With': 'XMLHttpRequest'
  }
})
