import { baseUrl, http } from "./httpService"
import { BlogResponse, IBlog } from "./interfaces"

const blogUrl = baseUrl + 'api/BlogPosts'

export const fetchBlogs = () => 
  http.get<BlogResponse>(blogUrl).then(response => response.data)

export const fetchBlogById = async (blogId: number) => {
  return http.get(blogUrl, {
      params: { id: blogId }
    })  
}

export const postBlog = (title: string, text:string) => {
  return http.post(blogUrl, {title,text})
}

export const editBlog = (blog:IBlog) => {
  return http.put(blogUrl + `/${blog.id}`, blog)
}

export const deleteBlog = async (id: number) => {
  return http.delete(blogUrl + `/${id}`)
}