export interface IBlog {
  id: number,
  title: string,
  createdAt:string
  text: string,
  categoryId: number
}

export interface BlogResponse {
  success: boolean,
  resultData: IBlog[]
  errorMessage: any
}