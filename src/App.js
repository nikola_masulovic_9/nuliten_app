import './App.css';
import NavBar from 'components/navBar/NavBar';
import HomeBlogScreen from 'screens/HomeBlogScreen';

function App() {
  return (
    <div className="App">
     <NavBar />
     <HomeBlogScreen />
    </div>
  );
}

export default App;
