import Blog from "components/blog/Blog";
import EditModal from "components/modal/EditModal";
import Modal from "components/modal/Modal";
import SideBar from "components/sideBar/SideBar";
import React, { useEffect, useState } from "react";
import { deleteBlog, fetchBlogs } from "service/blogService";
import { IBlog } from "service/interfaces";

const HomeBlogScreen: React.FC = () => {
  const [blogs, setBlogs] = useState<IBlog[]>();
  const [blogsChanged, setBlogsChanged] = useState(false);
  const [showModal, setShowModal] = useState(false);
  const [showEditModal, setShowEditModal] = useState(false);
  const [blogForEdit, setBlogForEdit] = useState<IBlog>(null)

  useEffect(() => {
    fetchBlogs().then((res) => setBlogs(res.resultData));
  }, [blogsChanged]);

  const deleteAction = (id: number) => {
    deleteBlog(id).then(() => {
      setBlogsChanged(!blogsChanged);
    });
  };

  const onClose = () => {
    setBlogsChanged(!blogsChanged);
    setShowModal(false)
    setShowEditModal(false)
  };

  const openModal = () => {
    setShowModal(true)
  }

  const openEditModal = (blog:IBlog) => {
    setShowEditModal(true)
    setBlogForEdit(blog)
  }



  return (
    <div className="content">
      <div className="top">
        <div className="msgs">
          {" "}
          Lorem Ipsum is simply dummy text of the printing and typesetting
        </div>
        <button onClick={() => openModal()}> Add post</button>
      </div>
      {showModal && <Modal onClose={onClose} />}
      {showEditModal && blogForEdit !== null && <EditModal blog={blogForEdit} onClose={onClose} />}
      <div className="lower">
        <SideBar></SideBar>
        <div className="postsList">
          {blogs &&
            blogs.map((blog) => {
              return (
                <Blog
                  key={blog.id}
                  blog={blog}
                  deleteAction={deleteAction}
                  editAction={() => openEditModal(blog)}
                />
              );
            })}
        </div>
      </div>
    </div>
  );
};

export default HomeBlogScreen;
